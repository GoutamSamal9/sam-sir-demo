import { useState } from 'react';
import { Demo } from './Demo';
import { Box, Button, makeStyles } from '@material-ui/core';
import SimpleCard from './Card';

const useStyle = makeStyles((theme) => ({
  container: {
    width: 500,
    height:'auto',
    margin: 'auto',
    background:theme.palette.common.black
  },
  btnOnOff: {
    background: 'green',
    '&:hover': {
      background:'yellow'
    }
  }
}));

function App() {
  const [count, setCount] = useState(0); //use state hook
  const classes = useStyle();

  const [switchkk, setSwitchkk]=useState(false)

  const handelClick = () => {
    console.log(count);
   setCount(count + 1)
  }
  const handelSwitch = (hh) => {
    setSwitchkk(hh);
  }

   const data = Array(100).fill("samsir");

  return (
    <Box className={classes.container}>
      <h1>click count {count}</h1>
      <Button
        className={classes.btnOnOff}
        color="primary"
        variant="contained"
        fullWidth
        style={{ padding: "20px" }}
        onClick={() => handelSwitch(!switchkk)}
      >
        {switchkk ? "off" : "On"}
      </Button>
      <Demo handelClick={(e) => handelClick(e)} />
      <Box mt={5}>
        {data.map((each, i) => (
          <SimpleCard each={each} />
        ))}
      </Box>
    </Box>
  );
}

export default App;
